// dimensional constants
var MARGIN = {TOP: 20, RIGHT: 30, BOTTOM: 320, LEFT: 80};
var WIDTH = 1920 - MARGIN.LEFT - MARGIN.RIGHT;
var HEIGHT = 1000 - MARGIN.TOP - MARGIN.BOTTOM;

// constants for data array
var MONTH = 0;
var TIME = 1;
var OUTCOME = 2;
var FORCE = 3;
var CITY = 4;
var CONTRABAND = 5;
var REASON = 6;
var RACE = 7;
var AGE = 8;
var SEX = 9;

// constants for month data
var JANUARY = 0;
var FEBRUARY = 1;
var MARCH = 2;
var APRIL = 3;
var MAY = 4;
var JUNE = 5;
var JULY = 6;
var AUGUST = 7;
var SEPTEMBER = 8;
var OCTOBER = 9;
var NOVEMBER = 10;
var DECEMBER = 11;

// constants for outcome data
var ARREST = 0;
var SUMMONS = 1;
var NEITHER = 2;

// constants for officer force data
var HANDS = 0;
var WALL = 1;
var GROUND = 2;
var DR_WEAPON = 3;
var PT_WEAPON = 4;
var BATON = 5;
var CUFFS = 6;
var SPRAY = 7;
var OTHER_FORCE = 8;

// constants for city data
var MANHATTAN = 0;
var BRONX = 1;
var QUEENS = 2;
var BROOKLYN = 3;
var STATEN_ISLAND = 4;

// constants for contraband data
var PISTOL = 0;
var RIFLE = 1;
var ASLT_WEAPON = 2;
var KNIFE = 3;
var MACH_GUN = 4;
var OTHER_WEA = 5;
var OTHER_CTRABAND = 5;

// constants for reason data
var SUSP_OBJECT = 0;
var RELE_DESCR = 1;
var CASING = 2;
var LOOKOUT = 3;
var CLOTHES = 4;
var DRUG_TRANS = 5;
var FURTIVE = 6;
var VIOLENT = 7;
var BULGE = 8;
var OTHER_REASON = 9;

// constants for race data
var NOT_LISTED = 0;
var ASIAN = 1;
var BLACK = 2;
var AMER_INDIAN = 3;
var BLACK_HISPANIC = 4;
var WHITE_HISPANIC = 5;
var WHITE = 6;
var UNKNOWN = 7;
var OTHER = 8;

// constants for sex data
var SEX_NOT_LISTED = 0;
var FEMALE = 1;
var MALE = 2;
var UNKNOWN_SEX = 3;

// global variables
var chart;
var bars;
var textValues;
var dataArray;
var monthData;
var timeData;
var outcomeData;
var forceData;
var cityData;
var contrabandData;
var reasonData;
var raceData;
var ageData;
var sexData;

// functions
function initData() {
    monthData = [];
    monthData[JANUARY] = {name:"JANUARY", value:0};
    monthData[FEBRUARY] = {name:"FEBRUARY", value:0};
    monthData[MARCH] = {name:"MARCH", value:0};
    monthData[APRIL] = {name:"APRIL", value:0};
    monthData[MAY] = {name:"MAY", value:0};
    monthData[JUNE] = {name:"JUNE", value:0};
    monthData[JULY] = {name:"JULY", value:0};
    monthData[AUGUST] = {name:"AUGUST", value:0};
    monthData[SEPTEMBER] = {name:"SEPTEMBER", value:0};
    monthData[OCTOBER] = {name:"OCTOBER", value:0};
    monthData[NOVEMBER] = {name:"NOVEMBER", value:0};
    monthData[DECEMBER] = {name:"DECEMBER", value:0};
    
    timeData = [];
    timeData[0] = {name:"12:00-12:59am", value:0};
    for (var i = 1; i < 12; i++) {
        var name = "" + i + ":00-" + i + ":59am";
        timeData[i] = {name:name, value:0};
    }
    timeData[12] = {name:"12:00-12:59pm", value:0};
    for (var i = 13; i < 24; i++) {
        var hr = i - 12;
        var name = "" + hr + ":00-" + hr + ":59pm";
        timeData[i] = {name:name, value:0};
    }
    
    outcomeData = [];
    outcomeData[ARREST] = {name:"ARREST MADE", value:0};
    outcomeData[SUMMONS] = {name:"SUMMONS ISSUED", value:0};
    outcomeData[NEITHER] = {name:"NEITHER", value:0};
    
    forceData = [];
    forceData[HANDS] = {name:"HANDS", value:0};
    forceData[WALL] = {name:"SUSPECT AGAINST WALL", value:0};
    forceData[GROUND] = {name:"SUSPECT ON GROUND", value:0};
    forceData[DR_WEAPON] = {name:"WEAPON DRAWN", value:0};
    forceData[PT_WEAPON] = {name:"WEAPON POINTED", value:0};
    forceData[BATON] = {name:"BATON", value:0};
    forceData[CUFFS] = {name:"HANDCUFFS", value:0};
    forceData[SPRAY] = {name:"PEPPER SPRAY", value:0};
    forceData[OTHER_FORCE] = {name:"OTHER", value:0};
    
    cityData = [];
    cityData[MANHATTAN] = {name:"MANHATTAN", value:0};
    cityData[BRONX] = {name:"BRONX", value:0};
    cityData[QUEENS] = {name:"QUEENS", value:0};
    cityData[BROOKLYN] = {name:"BROOKLYN", value:0};
    cityData[STATEN_ISLAND] = {name:"STATEN ISLAND", value:0};
    
    contrabandData = [];
    contrabandData[PISTOL] = {name:"PISTOL", value:0};
    contrabandData[RIFLE] = {name:"RIFLE", value:0};
    contrabandData[ASLT_WEAPON] = {name:"ASSAULT WEAPON", value:0};
    contrabandData[KNIFE] = {name:"KNIFE", value:0};
    contrabandData[MACH_GUN] = {name:"MACHINE GUN", value:0};
    contrabandData[OTHER_WEA] = {name:"OTHER WEAPON", value:0};
    contrabandData[OTHER_CTRABAND] = {name:"OTHER CONTRABAND", value:0};

    reasonData = [];
    reasonData[SUSP_OBJECT] = {name:"CARRYING SUSPICIOUS OBJECT", value:0};
    reasonData[RELE_DESCR] = {name:"FITS A RELEVANT DESCRIPTION", value:0};
    reasonData[CASING] = {name:"CASING A VICTIM OR LOCATION", value:0};
    reasonData[LOOKOUT] = {name:"ACTING AS A LOOKOUT", value:0};
    reasonData[CLOTHES] = {name:"WEARING CLOTHES COMMONLY USED IN A CRIME", value:0};
    reasonData[DRUG_TRANS] = {name:"ACTIONS INDICATIVE OF A DRUG TRANSACTION", value:0};
    reasonData[FURTIVE] = {name:"FURTIVE MOVEMENTS", value:0};
    reasonData[VIOLENT] = {name:"ACTIONS OF ENGAGING IN A VIOLENT CRIME", value:0};
    reasonData[BULGE] = {name:"SUSPICIOUS BULGE", value:0};
    reasonData[OTHER_REASON] = {name:"OTHER", value:0};
    
    raceData = [];
    raceData[NOT_LISTED] = {name:"NOT LISTED", value:0};
    raceData[ASIAN] = {name:"ASIAN/PACIFIC ISLANDER", value:0};
    raceData[BLACK] = {name:"BLACK", value:0};
    raceData[AMER_INDIAN] = {name:"AMERICAN INDIAN/ALASKAN NATIVE", value:0};
    raceData[BLACK_HISPANIC] = {name:"BLACK-HISPANIC", value:0};
    raceData[WHITE_HISPANIC] = {name:"WHITE-HISPANIC", value:0};
    raceData[WHITE] = {name:"WHITE", value:0};
    raceData[UNKNOWN] = {name:"UNKNOWN", value:0};
    raceData[OTHER] = {name:"OTHER", value:0};
    
    ageData = [];
    for (var i = 0; i < 10; i++) {
        var lowAge = (i * 10) + 10;
        var highAge = lowAge + 9;
        var ageRange = "" + lowAge + "-" + highAge;
        ageData[i] = {name:ageRange, value:0};
    }
    
    sexData = [];
    sexData[SEX_NOT_LISTED] = {name:"NOT LISTED", value:0};
    sexData[FEMALE] = {name:"FEMALE", value:0};
    sexData[MALE] = {name:"MALE", value:0};
    sexData[UNKNOWN_SEX] = {name:"UNKNOWN", value:0};
    
    dataArray = [];
    dataArray[MONTH] = monthData;
    dataArray[TIME] = timeData;
    dataArray[OUTCOME] = outcomeData;
    dataArray[FORCE] = forceData;
    dataArray[CITY] = cityData;
    dataArray[CONTRABAND] = contrabandData;
    dataArray[REASON] = reasonData;
    dataArray[RACE] = raceData;
    dataArray[AGE] = ageData;
    dataArray[SEX] = sexData;
}

function recordMonth(date) {
    var month;
    if (date.length === 7) {
        month = +date.slice(0, 1);
    } else {
        month = +date.slice(0, 2);
    }
    
    monthData[month - 1].value++;
}

function recordTime(time) {
    var hour;
    if (time.length === 2) {
        hour = 0;
    } else if (time.length === 3) {
        hour = +time.slice(0, 1);
    } else {
        hour = +time.slice(0, 2);
    }
    
   timeData[hour].value++;
}

function recordOutcome(am, si) {
    if (am === 'Y') outcomeData[ARREST].value++;
    else if (si === 'Y') outcomeData[SUMMONS].value++;
    else outcomeData[NEITHER].value++;
}

function recordForce(hands, wall, grnd, drwpn, ptwpn, baton, hcuff, pepsp, other) {
    if (hands === 'Y') forceData[HANDS].value++;
    if (wall === 'Y') forceData[WALL].value++;
    if (grnd === 'Y') forceData[GROUND].value++;
    if (drwpn === 'Y') forceData[DR_WEAPON].value++;
    if (ptwpn === 'Y') forceData[PT_WEAPON].value++;
    if (baton === 'Y') forceData[BATON].value++;
    if (hcuff === 'Y') forceData[CUFFS].value++;
    if (pepsp === 'Y') forceData[SPRAY].value++;
    if (other === 'Y') forceData[OTHER_FORCE].value++;
}

function recordCity(city) {
    if (city === 'MANHATTAN') cityData[MANHATTAN].value++;
    else if (city === 'BRONX') cityData[BRONX].value++;
    else if (city === 'QUEENS') cityData[QUEENS].value++;
    else if (city === 'BROOKLYN') cityData[BROOKLYN].value++;
    else if (city.startsWith('STATEN')) cityData[STATEN_ISLAND].value++;
    else alert('unknown city: ' + city);
}

function recordContraband(pistol, rifle, aw, knife, mg, ow) {
    if (pistol === 'Y') contrabandData[PISTOL].value++;
    if (rifle === 'Y') contrabandData[RIFLE].value++;
    if (aw === 'Y') contrabandData[ASLT_WEAPON].value++;
    if (knife === 'Y') contrabandData[KNIFE].value++;
    if (mg === 'Y') contrabandData[MACH_GUN].value++;
    if (ow === 'Y') contrabandData[OTHER_WEA].value++;
    if (pistol !== 'Y' && rifle !== 'Y' && aw !== 'Y' && knife !== 'Y'
            && mg !== 'Y' && ow !== 'Y') contrabandData[OTHER_CTRABAND].value++;
}

function recordReason(so, rd, casing, lookout, clothes, dt, furtive, violent,
        bulge, other) {
    if (so === 'Y') reasonData[SUSP_OBJECT].value++;
    if (rd === 'Y') reasonData[RELE_DESCR].value++;
    if (casing === 'Y') reasonData[CASING].value++;
    if (lookout === 'Y') reasonData[LOOKOUT].value++;
    if (clothes === 'Y') reasonData[CLOTHES].value++;
    if (dt === 'Y') reasonData[DRUG_TRANS].value++;
    if (furtive === 'Y') reasonData[FURTIVE].value++;
    if (violent === 'Y') reasonData[VIOLENT].value++;
    if (bulge === 'Y') reasonData[BULGE].value++;
    if (other === 'Y') reasonData[OTHER_REASON].value++;
}

function recordRace(race) {
    switch (race) {
        case 'A':
            raceData[ASIAN].value++;
            break;
        case 'B':
            raceData[BLACK].value++;
            break;
        case 'I':
            raceData[AMER_INDIAN].value++;
            break;
        case 'P':
            raceData[BLACK_HISPANIC].value++;
            break;
        case 'Q':
            raceData[WHITE_HISPANIC].value++;
            break;
        case 'W':
            raceData[WHITE].value++;
            break;
        case 'X':
            raceData[UNKNOWN].value++;
            break;
        case 'Z':
            raceData[OTHER].value++;
            break;
        default:
            raceData[NOT_LISTED].value++;
            break;
    }
}

function recordAge(age) {
    if (age >= 10 && age <= 108) {
        var i = Math.floor((age - 10) / 10);
        ageData[i].value++;
    }
}

function recordSex(sex) {
    switch(sex) {
        case 'F':
            sexData[FEMALE].value++;
            break;
        case 'M':
            sexData[MALE].value++;
            break;
        case 'Z':
            sexData[UNKNOWN_SEX].value++;
            break;
        default:
            sexData[SEX_NOT_LISTED].value++;
            break;
    }
}

function drawChart(data, title) {
    d3.selectAll("svg > *").remove();
    $('#chartTitle').text(title);
    maxVal = d3.max(data, function(d) { return d.value; });
    
    var domain = [];
    data.forEach(function(d) {
       domain.push(d.name); 
    });
    
    var yScale = d3.scale.linear()
            .domain([0, maxVal * 1.2])
            .range([HEIGHT, 0]);

    var xScale = d3.scale.ordinal()
            .domain(domain)
            .rangeBands([0, WIDTH], 0.5);
    
    var yAxis = d3.svg.axis()
            .scale(yScale)
            .orient('left');
    
    var xAxis = d3.svg.axis()
            .scale(xScale);

    chart = d3.select('.chart')
            .attr('width', WIDTH + MARGIN.LEFT + MARGIN.RIGHT)
            .attr('height', HEIGHT + MARGIN.TOP + MARGIN.BOTTOM)
            .append("g")
            .attr("transform", "translate(" + MARGIN.LEFT + "," + MARGIN.TOP + ")");
    
    chart.append('g')
            .attr("class", "x axis")
            .attr("transform", "translate(0," + HEIGHT + ")")
            .call(xAxis)
        .selectAll("text")
            .attr("transform", "rotate(-45) translate(-5, -5)")
            .style("text-anchor", "end");
    
    chart.append('g')
            .attr('class', 'y axis')
            .call(yAxis);
    
    bars = chart.selectAll('rect')
            .data(data)
            .enter().append('rect')
            .attr('width', xScale.rangeBand())
            .attr('x', function(d) {
                return xScale(d.name);
            })
            .attr('height', yScale(0))
            .attr('y', 0);
    
    textValues = chart.selectAll('text.value')
            .data(data)
            .enter().append('text')
            .style('visibility', 'hidden')
            .style('text-anchor', 'middle')
            .style('fill', 'white')
            .style('font', '18pt "Comic-Sans"')
            .attr('x', function(d) {
                return xScale(d.name) + xScale.rangeBand() / 2;
            })
            .attr('y', function(d) {
                return yScale(d.value + maxVal * 0.1) + 40;
            })
            .text(function(d) {
                return d.value;
            });
    
    bars.transition()
            .attr('height', function(d) {
                return yScale(0) - yScale(d.value);
            })
            .attr('y', function(d) {
                return yScale(d.value);
            })
            .duration(2000)
            .ease('bounce')
            .each('end', function() {
                textValues.style('visibility', 'visible');
            });
    
    
            
    bars.on('mouseover', function() {
        d3.select(this)
                .transition()
                .attr('width', xScale.rangeBand() + 20 )
                .attr('x', function(d) {
                    return xScale(d.name) - 10;
                })
                .attr('height', function(d) {
                    return yScale(0) - yScale(d.value + maxVal * 0.1);
                })
                .attr('y', function(d) {
                    return yScale(d.value + maxVal * 0.1);
                });
    });
    
    bars.on('mouseout', function() {
        d3.select(this)
                .transition()
                .attr('width', xScale.rangeBand())
                .attr('x', function(d) {
                    return xScale(d.name);
                })
                .attr('height', function(d) {
                    return yScale(0) - yScale(d.value);
                })
                .attr('y', function(d) {
                    return yScale(d.value);
                });
    });
}

$(document).ready(function() {
    initData();

    d3.csv('./data/2015_sqf_csv.csv', function(data) {
        data.forEach(function(row) {
            recordMonth(row.datestop);
            recordTime(row.timestop);
            recordOutcome(row.arstmade, row.sumissue);
            recordForce(row.pf_hands, row.pf_wall, row.pf_grnd, row.pf_drwep,
                    row.pf_ptwep, row.pf_baton, row.pf_hcuff, row.pf_pepsp,
                    row.pf_other);
            recordCity(row.city);
            if (row.contrabn === 'Y') recordContraband(row.pistol, row.riflshot,
                    row.asltweap, row.knifcuti, row.machgun, row.othrweap);
            recordReason(row.cs_objcs, row.cs_descr, row.cs_casng, row.cs_lkout,
                    row.cs_cloth, row.cs_drgtr, row.cs_furtv, row.cs_vcrim,
                    row.cs_bulge, row.cs_other);
            recordRace(row.race);
            recordAge(row.age);
            recordSex(row.sex);
        });
        
        drawChart(dataArray[RACE], 'Persons stopped by race');
    });
    
    $('select').change(function() {
        textValues.remove();
        bars.remove();
        
        $('select option:selected').each(function() {
            var title = $(this).text();
            var index = +$(this).val();
            drawChart(dataArray[index], title);
        });
    });
    
    $('#beforeButton').click(function() {
        var select = document.getElementById('attrSelect');
        select.selectedIndex = (select.selectedIndex - 1 > 0)
                ? select.selectedIndex - 1
                : 9;
        
        $('select option:selected').each(function() {
            var title = $(this).text();
            var index = +$(this).val();
            drawChart(dataArray[index], title);
        });
    });
    
    $('#nextButton').click(function() {
        var select = document.getElementById('attrSelect');
        select.selectedIndex = (select.selectedIndex + 1) % 10;
                
        $('select option:selected').each(function() {
            var title = $(this).text();
            var index = +$(this).val();
            drawChart(dataArray[index], title);
        });
    });
});
